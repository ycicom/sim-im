/*
 * uicommandcontext.h
 *
 *  Created on: Aug 8, 2011
 */

#ifndef UICOMMANDCONTEXT_H_
#define UICOMMANDCONTEXT_H_

namespace SIM
{
    namespace UiCommandContext
    {
        enum UiCommandContext
        {
            Nothing,
            Contact,
            Group,
            IMContact,
            IMGroup
        };
    }
}

#endif /* UICOMMANDCONTEXT_H_ */
