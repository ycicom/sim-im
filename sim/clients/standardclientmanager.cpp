
#include <algorithm>
#include <QDir>
#include "standardclientmanager.h"
#include "clientmanager.h"
#include "profile/profilemanager.h"
#include "contacts/protocolmanager.h"
#include "buffer.h"
#include "plugin/pluginmanager.h"

namespace SIM {

StandardClientManager::StandardClientManager(const ProfileManager::Ptr& profileManager, const ProtocolManager::Ptr& protocolManager) :
    m_protocolManager(protocolManager),
    m_profileManager(profileManager)
{
}

StandardClientManager::~StandardClientManager()
{
    //sync();
}

void StandardClientManager::addClient(ClientPtr client)
{
    log(L_DEBUG, "Adding client: %s", qPrintable(client->name()));
    m_clients.append(client);
}

ClientPtr StandardClientManager::client(const QString& name)
{
    ClientMap::iterator it = std::find_if(m_clients.begin(), m_clients.end(),
            [&] (const ClientPtr& client) {return client->name() == name;});
    if(it != m_clients.end())
        return *it;
    return ClientPtr();
}

ClientPtr StandardClientManager::client(int index)
{
    return m_clients.at(index);
}

QList<ClientPtr> StandardClientManager::allClients() const
{
    return m_clients;
}

bool StandardClientManager::load()
{
    log(L_DEBUG, "ClientManager::load()");
    m_clients.clear();
    if(!load_new())
    {
        if(!load_old())
            return false;
    }
    return true;
}

bool StandardClientManager::load_new()
{
    log(L_DEBUG, "ClientManager::load_new()");

    //QStringList clients = config()->rootPropertyHub()->value("Clients").toStringList();
    QStringList clients = config()->rootHub()->propertyHubNames();
    if (clients.size() == 0)
        return false;

    foreach(QString clientName, clients)
    {
        PropertyHubPtr clientProperty = config()->rootHub()->propertyHub(clientName);

        QString protocolName = clientProperty->value("protocol").toString();
        QString pluginName = clientProperty->value("plugin").toString();

        if(pluginName.isEmpty() || protocolName.isEmpty())
            return false;

        if(!getPluginManager()->isPluginProtocol(pluginName))
        {
            log(L_DEBUG, "Plugin %s is not a protocol plugin", qPrintable(pluginName));
            continue;
        }

        PluginPtr plugin = getPluginManager()->plugin(pluginName);
        if(plugin.isNull())
        {
            log(L_WARN, "Plugin %s not found", qPrintable(pluginName));
            continue;
        }

        ClientPtr newClient;
        m_profileManager->currentProfile()->enablePlugin(pluginName);

        for(int i = 0; i < m_protocolManager->protocolCount(); i++)
        {
            ProtocolPtr protocol = m_protocolManager->protocol(i);
            if (protocol->name() == protocolName)
            {
                newClient = protocol->createClient(clientName);
                addClient(newClient);
            }
        }

        newClient->loadState(clientProperty);
    }

    return true;
}

bool StandardClientManager::load_old()
{
    log(L_DEBUG, "ClientManager::load_old()");
    QString cfgName = m_profileManager->profilePath() + QDir::separator() + "clients.conf";
    QFile f(cfgName);
    if (!f.open(QIODevice::ReadOnly)){
        log(L_ERROR, "[2]Can't open %s", qPrintable(cfgName));
        return false;
    }

    Buffer cfg;
    ClientPtr client;
    while(!f.atEnd()) {
        QByteArray l = f.readLine();
        QString line = l.trimmed();
        log(L_DEBUG, "line: %s", qPrintable(line));
        if(line.startsWith("[")) {
            if(client) {
                cfg.setWritePos(cfg.size() - 1);
                client->deserialize(&cfg);
                addClient(client);
                cfg.clear();
                client.clear();
            }
            QString clientName = line.mid(1, line.length() - 2);
            QString pluginName = getToken(clientName, '/');
            if (pluginName.isEmpty() || clientName.length() == 0)
                continue;
            if(!getPluginManager()->isPluginProtocol(pluginName))
            {
                log(L_DEBUG, "Plugin %s is not a protocol plugin", qPrintable(pluginName));
                cfg.clear();    //Fixme: Make sure here, the dropped configuration converted later, if plugin is available again...
                client.clear();
                continue;
            }
            PluginPtr plugin = getPluginManager()->plugin(pluginName);
            if(plugin.isNull())
            {
                log(L_WARN, "Plugin %s not found", qPrintable(pluginName));
                continue;
            }
            m_profileManager->currentProfile()->enablePlugin(pluginName);

            for(int i = 0; i < m_protocolManager->protocolCount(); i++)
            {
                ProtocolPtr protocol = m_protocolManager->protocol(i);
                if (protocol->name() == clientName)
                {
                    cfg.clear();
                    client = protocol->createClient(0);
                }
            }
        }
        else {
            if(!l.isEmpty()) {
                cfg += l;
            }
        }
    }
    if(client) {
        cfg.setReadPos(0);
        cfg.setWritePos(cfg.size() - 1);
        client->deserialize(&cfg);
        addClient(client);
        cfg.clear();
        client.clear();
    }
    return m_clients.count() > 0;
}


bool StandardClientManager::sync()
{
    if(!m_profileManager || m_clients.isEmpty() )
        return false;
    log(L_DEBUG, "ClientManager::save(): %d", m_clients.count());

    config()->rootHub()->clearPropertyHubs(); //FIXME: deleted clients should delete corresponding propertyHubs during deletion
    foreach(const ClientPtr& client, m_clients)
    {
        config()->rootHub()->addPropertyHub(client->saveState());

        QString name = client->name();
        PropertyHubPtr hub = config()->rootHub()->propertyHub(name);
        hub->setValue("plugin",client->protocol()->plugin()->name());
        hub->setValue("protocol", client->protocol()->name());

//        client->properties()->serialize(el);
//        QDomElement clientDataElement = doc.createElement("clientdata");
//        client->serialize(clientDataElement);
//        el.appendChild(clientDataElement);
//        root.appendChild(el);
    }

    return config()->writeToFile();
}

ClientPtr StandardClientManager::createClient(const QString& name)
{
    QString pluginname = name.section('/', 0, 0);
    QString protocolname = name.section('/', 1, 1);

    if (pluginname.isEmpty() || protocolname.length() == 0)
        return ClientPtr();
    if(!getPluginManager()->isPluginProtocol(pluginname))
    {
        log(L_DEBUG, "Plugin %s is not a protocol plugin", qPrintable(pluginname));
        return ClientPtr();
    }
    PluginPtr plugin = getPluginManager()->plugin(pluginname);
    if(plugin.isNull())
    {
        log(L_WARN, "Plugin %s not found", qPrintable(pluginname));
        return ClientPtr();
    }
    m_profileManager->currentProfile()->enablePlugin(pluginname);
    ProtocolPtr protocol = m_protocolManager->protocol(protocolname);
    if(protocol)
        return protocol->createClient(name);
    log(L_DEBUG, "Protocol %s not found", qPrintable(protocolname));
    return ClientPtr();
}

QStringList StandardClientManager::clientList()
{
    QStringList result;
    foreach(const ClientPtr& client, m_clients)
    {
        result.append(client->name());
    }
    return result;
}

ClientPtr StandardClientManager::getClientByProfileName(const QString& name)
{
    foreach(const ClientPtr& client, m_clients)
    {
        if(client->name() == name)
            return client;
    }
    return ClientPtr();
}

void StandardClientManager::deleteClient(const QString& name)
{
    auto it = std::find_if(m_clients.begin(), m_clients.end(),
            [&] (const ClientPtr& client) {return client->name() == name; } );
    if(it != m_clients.end())
        m_clients.erase(it);
}

ConfigPtr StandardClientManager::config()
{
    if (!m_config.isNull() && m_loadedProfile == m_profileManager->currentProfileName())
        return m_config;

    m_loadedProfile = m_profileManager->currentProfileName();
    QString cfgName = m_profileManager->profilePath() + QDir::separator() + "clients.xml";
    log(L_DEBUG, "Creating config: %s", qPrintable(cfgName));
    m_config = ConfigPtr(new Config(cfgName));

    m_config->readFromFile();

    return m_config;
}

} // namespace SIM
