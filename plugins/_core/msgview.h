/***************************************************************************
                          msgview.h  -  description
                             -------------------
    begin                : Sun Mar 17 2002
    copyright            : (C) 2002 by Vladimir Shutoff
    email                : vovan@shutoff.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _MSGVIEW_H
#define _MSGVIEW_H

#include <vector>
#include <list>

#include <QTextEdit>
#include <QMenu>


#include "core_api.h"
#include "messaging/message.h"

class CorePlugin;
class XSL;

using namespace std;


class CORE_EXPORT MsgView : public QTextEdit
{
    Q_OBJECT
public:
    MsgView(QWidget *parent, int id = -1);
    virtual ~MsgView();

    void addMessage(const SIM::MessagePtr& message);
    int messageCount() const;
//    void		addMessage(SIM::Message *msg, bool bUnread=false, bool bSync=true);
//    bool		findMessage(SIM::Message *msg);
//    void		setSelect(const QString &str);
    void setXSL(XSL* xsl);
//    static		QString parseText(const QString &text, bool bIgnoreColors, bool bUseSmiles);
//    SIM::Message *currentMessage();
    int id() const;
protected slots:
    //void update();
protected:
//    virtual bool        processEvent(SIM::Event*);
//    virtual void        contextMenuEvent( QContextMenuEvent *event );
//    void setBackground(unsigned start);
//    void setSource(const QString&);
//    void setSource(const QUrl&);
//    void reload();
//    unsigned    messageId(const QString&, QString &client);
//    QString messageText(SIM::Message *msg, bool bUnread);
//    QPoint m_popupPos;
//    QString m_selectStr;
//    unsigned m_nSelection;
//    vector<CutHistory>	m_cut;
//    list<Msg_Id>		m_updated;
private:
    void refreshContent();
    QString makeHeader();
    QString printAllMessages();
    QString makeFooter();
    QString printMessage(const SIM::MessagePtr& message);

    QString quoteXml(const QString& text);

    QList<SIM::MessagePtr> m_messages;
    int m_id;
    XSL* m_xsl;

    QString m_xml;
};


#endif

